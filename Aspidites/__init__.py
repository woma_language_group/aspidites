__version__ = '0.11.1'
__title__ = 'Aspidites'
__author__ = 'Ross J. Duff'
__license__ = "GPL"
__description__ = (
    f"""
    {__title__} v{__version__} is the reference Woma programming language compiler.
    Copyright (C) 2021  {__author__}
    This program comes with ABSOLUTELY NO WARRANTY; 
    This is free software, and you are welcome to redistribute it
    under the conditions of the {__license__}v3 found in the LICENSE file.
    """
)
__mimetype__ = 'text/woma'
