.. Aspidites documentation master file, created by
   sphinx-quickstart on Sun Aug 15 16:48:46 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the Woma Programming Language!
=========================================

.. toctree::
   :numbered:
   :maxdepth: 4

   README
   syntax
   devinfo

